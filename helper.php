<?php

use Illuminate\Support\Collection;
use Illuminate\Support\Helpers;

class Vehicles
{
    protected $vehicles;
    protected $vehicle_groups;

    public function __construct()
    {
        $this->vehicles = $this->readJson('vehicles.json');
        $this->vehicle_groups = $this->readJson('vehicle_groups.json');
    }

    public function readJson($file)
    {
        $file = file_get_contents(__DIR__ . '/' . $file);

        return json_decode($file, true);
    }

    /*
    * YOUR FUNCTION HERE
    * */

    /*
        This function below should ideally live in a seperate class
        but for simplicity I will leave it here.

        This function is where my only foreach will live.
    */
    protected function collectRecursive($array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = $this->collectRecursive($value);
                $array[$key] = $value;
            }
        }

        return collect($array);
    }


    /*
        This is 'technically' my functions :)
    */

    protected $vehicles_col;
    protected $vehicles_group_col;

    public function processData()
    {
        $this->vehicles_col = $this->collectRecursive($this->vehicles);
        $this->vehicles_group_col = $this->collectRecursive($this->vehicle_groups);

        $vehicles = $this->getVehicles();

        $vehicle_statuses = $this->getVehicleStatuses($vehicles);

        $vehicle_count = collect([
            "total" => $vehicles->count()
        ]);

        $vehicle_statuses = $vehicle_statuses->merge($vehicle_count);

        return json_encode(
            [
                'vehicles' => $vehicles,
                'vehicle_stats' => $vehicle_statuses
            ]
        );
    }

    protected function getVehicleStatuses($vehicles)
    {
        return $vehicles->groupBy(function ($item, $key) {

            return $item['status'];

        })->mapWithKeys(function ($status_group, $key) {

            return [
                $key => $status_group->reduce(function ($carry, $item) {

                    return $carry + 1;

                })
            ];

        });
    }

    protected function getVehicles()
    {
        return $this->vehicles_col->mapWithKeys( function ($vehicle) {

            return collect([

                $vehicle['id'] => [
                    "lat" => (float) $vehicle['lat'],
                    "lng" => (float) $vehicle['lng'],
                    "speed" => (float) $vehicle['speed'],
                    "status" => (string) $vehicle['status'],
                    "groups" => $this->getVehicleGroup($vehicle['id'])
                ]

            ]);

        });
    }

    protected function getVehicleGroup($vehicle_id)
    {
        return $this->vehicles_group_col->filter( function($vehicle_group) use($vehicle_id) {

            return $vehicle_group['id'] === $vehicle_id;

        })->map(function($vehicle_group) {

            return explode(',', $vehicle_group['groups']);

        })->flatten()->map(function($vehicle_group_id) {

            return (int) $vehicle_group_id;

        });
    }

}
